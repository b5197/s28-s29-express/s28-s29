//[OBJECTIVE] Create a server-side app using Express Web Framework.

//Relate this task to something that you do on a daily basis.

//[SECTION] append the entire app to our node package manager
	//package.json -> the "heart" of every node project. this also contains different metadata that describes the structure of the project.
	//scripts -> is used to declare and describe custom commands and keyword that can be used to execute this project with the correct runtime environment.
	//NOTE: "start" is globally recognize amongst node projects and frameworks as the 'default' command script to execute a task/project.
	//however, for *unconventional* keywords or command, you have to append the command "run"
	//SYNTAX: np run <custom command> 

//1. Identify and prepare the ingredients.
const express = require("express"); 

//express -> main component to create the server.
//we need to gather/acquire the utilities and components needed that the express library will provide us.
	//=> require() -> directive used to get the library/component needed inside the module.

	//prepare the environment in which the project will be served.

//[SECTION] Preparing a Remote repository for our Node project.
	//NOTE: always DISABLE the node_modules folder
	//WHY?
		//-> it will take up too much space in our repository making it a lot more difficult to stage upon committing the changes in our remote repo.
		//-> if ever that you will deploy your node project on deployment platforms (heroku, netlify, vercel), the project will automatically be rejected, because node_modules is not recognized on various deployment platforms.
	//HOW? using a .gitignore module


//[SECTION] Create a Runtime environment that automatically autofix all the changes in our app.
	//we're going to use a utility called nodemon.


console.log(
`
Welcome to our Express API Server
───────────────────────────────────────
───▐▀▄───────▄▀▌───▄▄▄▄▄▄▄─────────────
───▌▒▒▀▄▄▄▄▄▀▒▒▐▄▀▀▒██▒██▒▀▀▄──────────
──▐▒▒▒▒▀▒▀▒▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄────────
──▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▄▒▒▒▒▒▒▒▒▒▒▒▒▀▄──────
▀█▒▒▒█▌▒▒█▒▒▐█▒▒▒▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌─────
▀▌▒▒▒▒▒▒▀▒▀▒▒▒▒▒▒▀▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐───▄▄
▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌▄█▒█
▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▒█▀─
▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▀───
▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌────
─▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐─────
─▐▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▌─────
──▌▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▐──────
──▐▄▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▄▌──────
────▀▄▄▀▀▀▀▀▄▄▀▀▀▀▀▀▀▄▄▀▀▀▀▀▄▄▀────────

`
	)

